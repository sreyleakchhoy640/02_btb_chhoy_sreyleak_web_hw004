import React, { Component } from "react";
import TableComponent from "./TableComponent";

export default class InputController extends Component {
  constructor() {
    super();
    this.state = {
      studentName: [
        { id: 1, email: "sreyleak@640gmail.com", stuName: "sreyleak", myage:"17", myaction:"doing exercise", isDone: false,},
        { id: 2, email: "Nita@123gmail.com", stuName: "Nita",myage: "3", myaction:"go to swim", isDone: false,},
        { id: 3, email: "phana555gmail.com", stuName: "phana",myage:"3",myaction:"making bread",isDone: true ,},
      ],
      newStu: "",
      newmail: "",
      newage: "",
      newaction:"",

    };
  }


  handleName = (event) => {
    this.setState({ newStu: event.target.value }, () =>
      console.log(event.target.value)
    );
  };

  handlmail = (event) => {
    this.setState({ newmail: event.target.value }, () =>
      console.log("mail",event.target.value)
    );
  };

  handlage = (event) => {
    this.setState({ newage: event.target.value }, () =>
      console.log("age",event.target.value)
    );
  };
  
  handlaction = (event) => {
    this.setState({ newaction: event.target.value }, () =>
      console.log("action",event.target.value)
    );
  };

  onSubmit = () => {
    console.log(this.state)
    const newObj = {
      id: this.state.studentName.length + 1,
      email: this.state.newmail,
      stuName: this.state.newStu,
      myage: this.state.newage,
      myaction: this.state.newaction,


    };
    console.log(newObj)
    this.setState(
      { studentName: [...this.state.studentName, newObj] },
      () => console.log("New Array StudentName=", this.state.studentName)
    );
  };

  onButtonChange = (id) => {
    console.log("id : ", id);

    this.state.studentName.map((data) => {
      if (data.id == id) {
        data.isDone=!data.isDone;
      }
    });
    this.setState({
      data: this.state.studentName,
    });

   
  };

  render() {
    return (
     
      <div class="m-20  ">
         <h1 className="font-bold text-2xl text-lime-500"> Please fill your information </h1><br />
        <label
          for="input-group-1"
          class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
        >
          Your Email
        </label>
        <div class="relative mb-6">
          <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-gray-500 dark:text-gray-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
            </svg>
          </div>
          <input
            type="text"
            id="input-group-1"
            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="name@flowbite.com"
            onChange={this.handlmail}
          />
        </div>
        <label
          for="website-admin"
          class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
        >
          Username
        </label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-blue-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            @
          </span>
          <input
            type="text"
            id="website-admin"
            class="rounded-none rounded-r-lg bg-gray-50 border text-blue-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="elonmusk"
            onChange={this.handleName}
          />
        </div>
        <br />
        
        <div class="mb-6">
  <label for="success" class="block mb-2 text-sm font-medium text-green-700 dark:text-green-500">Your Age</label>
  <input type="text" id="success" class="bg-green-50 border border-green-500 text-green-900 dark:text-green-400 placeholder-green-700 dark:placeholder-green-500 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5 dark:bg-gray-700 dark:border-green-500" placeholder="Success input"
  onChange={this.handlage}/>
  

</div>

<div class="mb-6">
    <label for="large-input" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Discribe your action </label>
    <input type="text" id="large-input" class="block w-full p-4 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
     onChange={this.handlaction}/>
      <p class="mt-2 text-sm text-green-600 dark:text-green-500"><span class="font-medium">Well done!</span> Some success message.</p>
</div>


        <button
          onClick={this.onSubmit}
          class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        >
          // Submit //{" "}
        </button>
       <br /><br /><br />
        <TableComponent data={this.state.studentName} 
         onButtonChange={this.onButtonChange}
        />
        
       

      </div>
    );
  }
}
