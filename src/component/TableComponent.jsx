import React, { Component } from "react";
import Swal from 'sweetalert2'
import 'animate.css';

export default class TableComponent extends Component {
    Alert = (data) =>{
        Swal.fire({

            title: "ID:" + data.id + "\n" + "Email:" + data.Email + "\n" + "Name:" + data.stuName + "\n" + "Age:" + data.Age,
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
          })
    }

  render() {
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg bg-cyan-200">
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase dark:text-gray-400">

              <tr class="h-20 w-2/5 bg-gradient-to-r from-purple-500 to-pink-500">
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Email
                </th>
                <th scope="col" class="px-6 py-3">
                  Username
                </th>
                <th scope="col" class="px-6 py-3">
                  Age
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr class="border-b border-gray-200 dark:border-gray-700">
                  <td class="px-6 py-3">{item.id}</td>
                  <td class="">{item.email}</td>
                  <td class="px-6 py-3">{item.stuName}</td>
                  <td class="px-6 py-3">{item.myage}</td>

                  <td class="px-6 py-3">{item.myaction}</td>
                  <td>
                    {" "}
                    <button
                    onClick={()=>{this.props. onButtonChange(item.id)}}
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ">
                      {item.isDone?"done":"pending"}
                    </button>
                    <button onClick={()=> this.Alert(item) } class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-4">
                      showmore
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
